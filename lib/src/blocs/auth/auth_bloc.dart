import 'dart:async';
import 'package:rxdart/rxdart.dart';
import '../base_provider/base_bloc.dart';
import '../../utils/utils.dart';
import '../repository/repositories.dart';

class AuthBloc with Validator implements BaseBloc {

  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();
  final authRepository = AuthFirebaseRepository();

  Function(String) get changeEmail => _email.sink.add;
  Function(String) get changePassword => _password.sink.add;

  Stream<String> get email => _email.stream.transform(validateEmail);
  Stream<String> get password => _password.stream.transform(validateEmpty);
  
  Stream<bool> get submit => Observable.combineLatest2(email, password, (e,p) => true);

   void signIn() {
     if (_email.value != null && _password.value != null) {
         authRepository.signIn(_email.value, _password.value);
     }else {
       _email.sink.addError('This is required'); 
       _password.sink.addError('This is required');  
       }

  }

  @override
  void dispose() {
    _email.close();
    _password.close();
    print('disposing!');
  }



}