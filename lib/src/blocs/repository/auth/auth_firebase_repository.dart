import './auth_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';


class AuthFirebaseRepository implements AuthRepository{
  
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Future<String> getCurrentUser() {
    // TODO: implement getCurrentUser
    return null;
  }

  @override
  Future<String> signIn(String email, String password) {
    _firebaseAuth.signInWithEmailAndPassword(email: email, password: password).then( (user) {
        print('user ${user.email}');
        return user.uid;
    }).catchError( (e) => print(e));

  }

  @override
  Future<void> signOut() {
    // TODO: implement signOut
    return null;
  }

  @override
  Future<String> signUp(String email, String password) {
    // TODO: implement signUp
    return null;
  }

}