import 'dart:async';

mixin Validator{
  static Pattern emailRegex = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

  final validateEmail = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink){
      RegExp regex = new RegExp(emailRegex);
      if (!regex.hasMatch(email))
        sink.addError('Invalid Email');
      else{
        sink.add(email);

      }
    },
  );

  final validateEmpty  =StreamTransformer<String, String>.fromHandlers(

    handleData: (value, sink) {
      if(value.isEmpty){
        sink.addError('This is required');
      }
      else{
        sink.add(value);
      }
    }
  );

}