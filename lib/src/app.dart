import 'package:flutter/material.dart';
import 'blocs/blocs.dart';
import 'ui/screens.dart';

class App extends StatelessWidget{
  // TODO: cleaning function for bloc builders
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'GlovoFlutter',
      home: BlocProvider<AppBloc>(
        builder: (_,bloc) => bloc ?? AppBloc(),
        onDispose: (_,bloc) => bloc.dispose(),
        child: BlocProvider<AuthBloc>(
            builder: (_,bloc) => bloc ?? AuthBloc(),
            onDispose: (_,bloc) => bloc.dispose(),
            child: SignIn()) ,
      ),
    );

  }

}