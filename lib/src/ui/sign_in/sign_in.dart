import 'package:flutter/material.dart';
import '../../blocs/blocs.dart';

class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<AuthBloc>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('Sign In'),
          backgroundColor: Colors.blue,
        ),
        body: Container(
            decoration: BoxDecoration(color: Colors.white),
            padding: EdgeInsets.all(20.0),
            child: 
            StreamBuilder( stream: bloc.submit,
            builder: (context, submitSnaphsot) {
              return  Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                StreamBuilder(
                    stream: bloc.email,
                    builder: (context, snapshot) {
                      return TextField(
                        decoration: InputDecoration(
                            hintText: 'Username', errorText: submitSnaphsot.hasError ? snapshot.error : null),
                        onChanged: bloc.changeEmail,
                      );
                    }),
                SizedBox(height: 20.0),
                StreamBuilder(
                    stream: bloc.password,
                    builder: (context, snapshot) {
                      return TextField(
                        decoration: InputDecoration(
                            hintText: 'Password', errorText: submitSnaphsot.hasError ? snapshot.error : null),
                        obscureText: true,
                        onChanged: bloc.changePassword,
                      );
                    }),
                SizedBox(height: 20.0),
                RaisedButton(
                  color: Colors.black,
                  textColor: Colors.white,
                  child: Text('Sign In'),
                  onPressed: !submitSnaphsot.hasError ? bloc.signIn : (){},
                )
              ],
            );
            }
           )));
  }
}
